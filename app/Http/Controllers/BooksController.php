<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BooksController extends Controller
{
    
    /** aggiunge un nuovo autore */
    public function add(Request $request)
    {
        $title = $request->input('title');
        //$genere = $request->input('genere');
        $year = $request->input('year');
        $price = $request->input('price');
                
        $results = app('db')->insert(
            "INSERT INTO books(title,`year`, price) VALUES('$title','$year', '$price')"
        );
        // ritorno 201 created
        return new Response(null, 201);
    }


    /** aggiorno un autore esistente per id */
    public function update(Request $request,$id)
    {
        $title = $request->input('title');
        $genere = $request->input('genere');
        $year = $request->input('year');
        $price = $request->input('price');
                
        $results = app('db')->update(
            "UPDATE books SET title='$titel', genere='$genere', `year`='$year', price='$price' WHERE id=$id"
        );
        // ritorno 200 OK
        return new Response(null, 200);
    }

       /** elimino un autore esistente per id */
       public function delete(Request $request,$id)
       {
                  
           $results = app('db')->delete(
               "DELETE FROM books WHERE id=$id"
           );
           // ritorno 204 no content
           return new Response(null, 204);
       }

}
