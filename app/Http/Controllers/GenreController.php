<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GenreController extends Controller
{
    
    /** aggiunge un nuovo autore */
    public function add(Request $request)
    {
        $genre = $request->input('genre');
        $description = $request->input('description');
                
        $results = app('db')->insert(
            "INSERT INTO genres(genre,description) VALUES('$genre','$description')"
        );
        // ritorno 201 created
        return new Response(null, 201);
    }


    /** aggiorno un autore esistente per id */
    public function update(Request $request,$id)
    {
        $genre = $request->input('genere');
        $description = $request->input('description');
                
        $results = app('db')->update(
            "UPDATE genre SET genre='$genre', description='$description' WHERE id=$id"
        );
        // ritorno 200 OK
        return new Response(null, 200);
    }

       /** elimino un autore esistente per id */
       public function delete(Request $request,$id)
       {
                  
           $results = app('db')->delete(
               "DELETE FROM genre WHERE id=$id"
           );
           // ritorno 204 no content
           return new Response(null, 204);
       }

}
